package com.woniuxy.yuekeadmin.service.impl;

import com.woniuxy.yuekeadmin.model.User;
import com.woniuxy.yuekeadmin.dao.UserMapper;
import com.woniuxy.yuekeadmin.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Freedom
 * @since 2020-11-02
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
