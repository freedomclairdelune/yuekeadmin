package com.woniuxy.yuekeadmin.service;

import com.woniuxy.yuekeadmin.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Freedom
 * @since 2020-11-02
 */
public interface IUserService extends IService<User> {

}
