package com.woniuxy.yuekeadmin.dao;

import com.woniuxy.yuekeadmin.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Freedom
 * @since 2020-11-02
 */
public interface UserMapper extends BaseMapper<User> {

}
