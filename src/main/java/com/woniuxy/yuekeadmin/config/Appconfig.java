package com.woniuxy.yuekeadmin.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.woniuxy.yuekeadmin.common.EncryptUtil;
import com.woniuxy.yuekeadmin.component.ShiroRealm;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Appconfig {
    /**
     * 新的分页插件,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存出现问题
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.H2));
        return interceptor;
    }
    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return configuration -> configuration.setUseDeprecatedExecutor(false);
    }
    @Bean
    public CredentialsMatcher hashedCredentialsMatcher(){
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(EncryptUtil.ALGORITHM_NAME);
        matcher.setHashIterations(EncryptUtil.HASH_ITERATIONS);
        return matcher;
    }
    @Bean
    public Realm shiroRealm(){
        ShiroRealm shiroRealm =new ShiroRealm();
        shiroRealm.setCredentialsMatcher(hashedCredentialsMatcher());
        return shiroRealm;
    }
    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition(){
        DefaultShiroFilterChainDefinition sfcd= new DefaultShiroFilterChainDefinition();
        sfcd.addPathDefinition("/","anon");
        sfcd.addPathDefinition("/login","anon");
        sfcd.addPathDefinition("/css/**","anon");
        sfcd.addPathDefinition("/js/**","anon");
        sfcd.addPathDefinition("/imgs/**","anon");
        sfcd.addPathDefinition("/html","anon");
        sfcd.addPathDefinition("/logout","logout");
        sfcd.addPathDefinition("/checkusername","anon");
        sfcd.addPathDefinition("/zhuce","anon");
        sfcd.addPathDefinition("/getcode","anon");
        sfcd.addPathDefinition("/user.html","anon");
//        sfcd.addPathDefinition("/error","anon");
        sfcd.addPathDefinition("/**","user");
        return sfcd;
    }
}
