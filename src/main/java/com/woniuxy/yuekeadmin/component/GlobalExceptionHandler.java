package com.woniuxy.yuekeadmin.component;


import com.woniuxy.yuekeadmin.common.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

//全局异常处理
@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result handleException(Exception e){

        e.printStackTrace();

        return Result.fail("服务器开了小差，程序员小哥正在努力修复，具体信息【"+e.getMessage()+"】");
    }
}
