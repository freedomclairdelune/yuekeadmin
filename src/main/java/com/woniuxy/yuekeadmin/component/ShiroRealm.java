package com.woniuxy.yuekeadmin.component;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class ShiroRealm extends AuthorizingRealm {
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if(principals==null){
            throw  new RuntimeException("该账号不存在");
        }
//        User user = (User) principals.getPrimaryPrincipal();
//        Set<String> partset = loginService.findPartByUserId(user.getUserId());
//        Set<String> permissions = loginService.findpermissionsByUserId(user.getUserId());
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
//        simpleAuthorizationInfo.setStringPermissions(permissions);
//        simpleAuthorizationInfo.setRoles(partset);
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
//        UsernamePasswordToken usertoken=(UsernamePasswordToken) token;
//        User user = loginService.queryUserByUserName(usertoken.getUsername());
//        System.out.println("================================================================="+user);
//        return new SimpleAuthenticationInfo(user,user.getPassword().toCharArray(),getName());
        return new SimpleAuthenticationInfo(null,null,getName());
    }
}
