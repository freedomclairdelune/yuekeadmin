package com.woniuxy.yuekeadmin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.woniuxy.yuekeadmin.dao")
public class YuekeadminApplication {

    public static void main(String[] args) {
        SpringApplication.run(YuekeadminApplication.class, args);
    }

}
