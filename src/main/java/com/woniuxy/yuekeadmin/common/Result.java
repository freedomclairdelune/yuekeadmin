package com.woniuxy.yuekeadmin.common;

import lombok.Data;

@Data
public class Result {
    private boolean success;
    private String message;
    private Object data;

    public Result() {
    }

    public Result(boolean success, String message, Object data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }
    public static Result succes(){
        return succes(null);
    }

    public static Result succes(Object data){
        return new Result(true,"执行成功",data);
    }
    public static Result fail(String message){
        return new Result(false,message,null);
    }

}
